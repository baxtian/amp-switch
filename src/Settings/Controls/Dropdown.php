<?php
namespace AmpSwitch\Settings\Controls;

use AmpSwitch\Init as AmpSwitch;
use WP_Customize_Control;

/**
* Crear control para hacer selector desplegable
*/
class Dropdown extends WP_Customize_Control
{
	// Nombre del tipo con el que se representará
	public $type = 'dropdown';

	/**
	 * Renderizar el contenido
	 */
	public function render_content()
	{
		if (empty($this->choices)) {
			return;
		}

		$args = array(
			'id' => $this->id,
			'label' => esc_attr($this->label),
			'description' => esc_attr($this->description),
			'link' => $this->get_link(),
			'value' => esc_textarea($this->value()),
			'choices' => $this->choices,
		);
		AmpSwitch::render('controls/dropdown.twig', $args, true);
	}

	/**
	 * Función para sanear el valor
	 * @param  string|int $value Valor a sanera
	 * @return int        Valor saneado
	 */
	public static function sanitize($value)
	{
		$value = (int)$value;
		return $value;
	}
}
