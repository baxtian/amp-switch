# AMP-Switch

**Colaboradores:** sebaxtian  
**Requiere al menos:** WordPress 4.4  
**Probado hasta:** WordPress 5.2.2
**Licencia:** GPLv2 or later  
**Enlace a la licencia:** http://www.gnu.org/licenses/gpl-2.0.html  
**Etiquetas:** amp, theme, switch


Determina la plantilla que será tenida en cuenta como AMP y el atributo en la URL con el que indicará
el uso de esa plantilla.

## Description

Usar el ajuse de apariencia para determinar cuál plantilla usar cuando se use el elemento asigado a la
URL para llamar la plantilla AMP. Dejar vacío este elemento para no usar la plantilla AMP.

EL plugin agregará la metaeqiqueta que declara el enlace a la versión AMP.

Las opciones para para determinar el uso de la plantilla serán:

1. Subdominio (ToDo) (amp.dominio.com)
2. Directorio en el path (ToDo) dominio.com/path/amp/
3. Variable en la consulta dominio.com/path/?amp

## Installation

1. Decompress scss-library.zip and upload `/scss-library/` to the `/wp-content/plugins/` directory.
2. Activate the plugin through the __Plugins__ menu in WordPress.

## Frequently Asked Questions

# Does this plugin converts any theme into an AMP capable theme?

No. This theme enables your site to use a theme as an AMP. It does not converts your theme.
It is up to you to create an AMP theme.

# Is this plugin bug free?
I don't think so. Feedbacks would be appreciated.

## Changelog
# 0.1
* First release.
