<?php
namespace AmpSwitch\Settings;

/**
 * Componentes y controles para el panel de personalización de controles
 */
class AmpSwitch
{
	// Instancia
	protected static $instance = null;

	/**
	 * Retornar la instancia única de este componente
	 */
	public static function get_instance(){
		if( null == self::$instance ){
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Inicializa el componente
	 */
	protected function __construct()
	{
		add_action('customize_register', array($this, 'options'));
	}

	/**
	 * Agregar el panel a la pantalla de administración y persoanlización
	 * @param  WP_Customize_Manager $wp_customize Instancia del controlador del personalizador
	 */
	public function options($wp_customize)
	{
		// Agregar sección de Redes sociales
		$wp_customize->add_section(
			'ampswitch',
			array(
				'title' => __('AMP Switch', ASW_D),
			)
		);

		// Declarar el campo para selector del tema
		$wp_customize->add_setting(
			'ampswitch[theme]',
			array(
				'type' => 'option', // or 'theme_mod'
				// 'sanitize_callback' => '\AmpSwitch\Settings\Controls\Dropdown::sanitize',
			)
		);

		// Usar listado de temas como elementos del selector
		$_themes = wp_get_themes(array( 'allowed' => true ));
		$choices = array(
			'0' => __('&mdash; Do not assign &mdash;', ASW_D)
		);
		foreach ($_themes as $key => $item) {
			$choices[ $key ] = $item->get('Name');
		}
		asort($choices);

		$wp_customize->add_control(
			new \AmpSwitch\Settings\Controls\Dropdown(
				$wp_customize,
				'ampswitch[theme]',
				array(
					'label' => __('Theme', ASW_D),
					'description' => __('Use this as the AMP theme.', ASW_D),
					'section' => 'ampswitch',
					'settings' => 'ampswitch[theme]',
					'type' => 'dropdown',
					'choices' => $choices
				)
			)
		);

	}
}
