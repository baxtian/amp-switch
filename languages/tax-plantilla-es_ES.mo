��    n      �  �   �      P	  "   Q	     t	  U   �	     �	     �	     
     
     /
     7
     E
     S
     \
  
   k
     v
     �
     �
     �
  	   �
     �
  -   �
     �
       +     
   ?     J     Q     k          �     �     �  
   �  
   �  	   �     �     �     �  U   �  Y   H     �     �  b   �               &     4     S     a     {     �     �     �     �     �     �  
   �     �  4   �                    $     )  	   8     B     N  
   W     b     g     j     �     �     �     �     �     �     �     �     �  �   �     �  2   �     �                    *     6     E     N     `     h     m     y     �     �     �     �  �   �     �     �     �  
   �  	   �     �     �                 _  -      �     �  o   �     .     I     e     l     �     �     �     �     �     �     �          *     J  
   Y  
   d  6   o     �     �  B   �            &        D     _     l     u  $   �     �     �     �     �  	   �  
   �  r   �  l   g     �     �  |   �     b     i       )   �     �     �     �     �     	            	        )     ?  
   U  H   `     �     �     �     �     �     �     �               '     0  *   3  /   ^      �     �     �     �     �     �     �     
  �        �  D   �  	             "     2     D     S     j     w     �     �     �     �     �     �     �     �  �        �     
          6     D     P     T  
   k     v     x         m   L      R   ,   1          ^   i       6             !      *   
      X                 =   B   M       b   O   2             k   ?      c      Z         "   0   E      S       +   U   j           &       Y   H       Q           T       <          N   ]   -       d   4   I   :      l      J   _   f       C           \   K   a         F   A          e   P       G   7   5              %   `   3   /   8   9          n       (      h   >   V              	                      ;           .   g          $       D      @       W   )       '       [   #    %s has been activated succesfully. %s requires %s. %s requires %s. Use this link to automatically <a href='%s'>install requirements</a>. &mdash; Do not assign &mdash; &mdash; No Change &mdash; Active Add images to gallery Add new Add new genre Add new movie Add news Add to gallery All genres Anonymous user Aprobe Team Member Assign data sheet Author or Source Biography Category Click on the image to edit or update the logo Color Controls example Couldn't install %s. Pleas try it manually. Data Sheet Delete Delete link to data sheet Delete link to logo Description Director Distributor Edit Team Member Block Edit genre Edit movie Edit news Etiqueta Featured Finished For reference you can export a CSV file with all %s using <a href='%s'>this link</a>. For reference you can export a CSV file with all movies using <a href='%s'>this link</a>. Genre Genres If the import time of a page takes longer than expected, try again until you get the final result. Image Image gallery Import Movies Import Movies from a CSV file. Is a musical? Is this a featured movie? It is a musical It is treated by Linkedin Logo Message Movie Movie Archive Movie data Movies Movies added: %d, Movies updated: %d, Terms added %d Music Music by Musical Name New genre name New movie New movies: New news New terms: News No No books found in Trash. No movies found in Trash. No movies found. No news found. Page Pages Parent genre Parent genre: Parent movies: Person Please select the file with the information and click on <b>%s</b>. The system will inform each process that is executed during the import and in the end will expose information with the imported elements. Post Posts added: %d, Posts updated: %d, Terms added %d Producer Roles Search genres Search movies Search news Set data sheet Set logo Show in all pages Staring Step Synopsis... Talks about Team Member Team member The parameter was not Boolean. This is not a CSV file. This process imports a new record for each row of the CSV file and uses the data in the first row to determine the field corresponding to each column. Remember to add at least the <b>post_title</b> column. Unregistered visitor Update genre Updated movies: View movie View news Yes You must upload a file. movies tag delimiter, — Homepage — Project-Id-Version: Tax-Plantilla
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-09-11 09:34-0500
PO-Revision-Date: 2019-09-11 09:34-0500
Last-Translator: Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>
Language-Team: Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;esc_attr_e;esc_attr__
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 2.2.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
 %s se ha activado correctamente. %s requiere %s. %s requiere %s. Use el siguiente enlace para <a href='%s'>instalar los requerimientos</a> en forma automática. &mdash; No asignar &mdash; &mdash; Sin cambios &mdash; Activo Agregar imágenes a la galería Añadir nueva Agregar nuevo género Añadir nueva película Agregar noticia Agregar a galería Todos los géneros Usuario anónimo Aprobar Miembro del Equipo Asignar archivo de información Autor o Fuente Biografía Categoría Haz clic en la imagen para editar o actualizar el logo Color Ejemplo con Controles No se ha podido instalar %s. Por favor inténtelo en forma manual. Ficha técnica Eliminar Eliminar el enlace a la ficha técnica Eliminar el enlace al logo Descripción Director Distribuidor Editar bloque del Miembro del Equipo Editar género Editar película Editar noticia Etiqueta Destacada Finalizado Para referencia, puede exportar un archivo CSV con toda la información de %s usando <a href='%s'>este enlace</a>. Para referencia, puede exportar un archivo CSV con todas las películas usando <a href='%s'>este enlace</a>. Género Géneros Si el tiempo de importación de una página demora más de lo esperado, intente nuevamente hasta obtener el resultado final. Imagen Galería de imágenes Importar películas Importar Películas desde un archivo CSV. ¿Es un musical? ¿Es una película destacada? Es un musical Es tratada por Linkedin Logo Mensaje Película Archivo de películas Datos de la Película Películas Películas nuevas: %d, Películas actualizadas: %d, Términos nuevos: %d Música Música por Musical Nombre Nombre de nuevo género Nueva película Películas nuevas: Nueva noticia Nuevos términos: Noticias No No se encontraron noticias en la papelera. No se han encontrado películas en la papelera. No se han encontrado películas. No se encontraron noticias. Página Páginas Género padre Género padre: Película padre: Persona Seleccione el archivo con la información y haga clic en <b>%s</b>. El sistema informará cada paso ejecutado y al final expondrá información sobre los elementos importados. Entrada Entradas nuevas: %d, Entradas actualizadas: %d, Términos nuevos: %d Productor Perfiles Buscar géneros Buscar películas Buscar noticia Asignar ficha técnica Asignar logo Mostrar todas las páginas Actores Paso Sinopsis... Habla de Miembro del Equipo Miembro del Equipo El parámetro no era booleano. Este no es un archivo CSV. Este proceso importa un nuevo registro para cada fila del archivo CSV y utiliza los datos en la primera fila para determinar el campo correspondiente a cada columna. Recuerde agregar al menos la columna <b>post_title</b>. Visitante no registrado Actualizar género Películas actualizadas: Ver película Ver noticia Sí Debe subir un archivo. películas , — Página de inicio — 