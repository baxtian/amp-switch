<?php

namespace AmpSwitch;

use Timber;
use Twig_SimpleFilter;

// Usar cualquier componente de taxonomía, bloque o ajuste requerido por el plugin
use AmpSwitch\Settings\AmpSwitch as Settings;

//Inicializar taxonomía, bloque o ajuste requerido por el plugin
Settings::get_instance();

// Inicializar Plugin
class Init
{
	// Opciones
	private $tema_amp;

	// Instancia
	protected static $instance = null;

	/**
	 * Retornar la instancia única de este componente
	 */
	public static function get_instance()
	{
		if (null == self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Inicializa el componente
	 */
	protected function __construct()
	{
		// FIltro para incluir la variable
		add_filter('query_vars', [$this, 'query_vars']);

		// ¿Está asignado el tema?
		$opciones = get_option('ampswitch');
		$this->tema_amp = (
			empty($opciones) || // Aun no se han asignado opciones
			!isset($opciones['theme']) || // Aun no está asignada la opción del tema
			$opciones['theme'] == '0' // Está asignada la opción '0' (no asignar aún)
		) ? false : $opciones['theme'];

		//Si no estamos en el admin
		if (!is_admin()) {
			// Si hay un tema asignado, aplicar los filtros
			if ($this->tema_amp) {
				// Si está el 'disparador'
				if (isset($_GET['amp'])) {				
					add_filter('pre_option_stylesheet', [ $this, 'set_theme' ]);
					add_filter('pre_option_template', [ $this, 'set_theme' ]);
				} else {
					add_action('wp_head', [ $this, 'add_meta' ]);
				}
			}
		}
	}

	/**
	 * Incluir la variable amp enntre las variables de búsqueda
	 * @param  array $vars Arreglo con variables
	 * @return array       Arreglo de variables ahora con las de este plugin
	 */
	public function query_vars($vars)
	{
		$my_vars = array(
			'amp',
		);

		return array_merge($my_vars, $vars);
	}

	/**
	 * Retornar la URL del directorio de assets
	 * @return string
	 */
	public function set_theme($theme)
	{
		return $this->tema_amp;
	}

	/**
	 * Desparsea un rreglo con los datos de una URL
	 * @param  array $parsed_url Arregloc on los datos de la URL.
	 * 							 Ver parse_url para conocer los elementos del arreglo.
	 * @return string            Cadena con la URL
	 */
	private function unparse_url($parsed_url)
	{
		$scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
		$host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
		$port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
		$user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
		$pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
		$pass     = ($user || $pass) ? "$pass@" : '';
		$path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
		$query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
		$fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
		return "$scheme$user$pass$host$port$path$query$fragment";
	}

	/**
	 * Añadir el metaheader
	 * @return string
	 */
	public function add_meta()
	{
		// Obtener la URL completa
		$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
				"https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
				$_SERVER['REQUEST_URI'];
		$_url = parse_url($url);

		// Incluir AMP en el query
		parse_str($_url['query'], $params);
		$params['amp'] = true;
		$_url['query'] = http_build_query($params);

		printf("<link rel='amphtml' href='%s'>", $this->unparse_url($_url));
	}

	/**
	 * Función para renderizar una plantilla.
	 * Requiere tener activo el plugin Timber.
	 * Lea la documentación de Timber en http://timber.github.io/timber/
	 * Timber está basado en Twig.
	 * Lee la documentación de Twig en https://twig.sensiolabs.org/doc/2.x/
	 * @param  string  $tpl   Plantilla twig a ser usada
	 * @param  array   $args  Arregloc on los argumentos a ser enviados a la plantilla
	 * @param  boolean $echo  Si desea capturar la salida del render use el parámetro echo. Si es false devolverá
	 * 						  la información directamente en vez de imprimirla. Por defecto es true.
	 * @return boolean|string Si echo es true indica si pudo o no renderizar la plantilla. Si es false retornará la
	 * 						  cadena con el texto renderizado o false si no pudo ejecutar la acción
	 */
	public static function render($tpl, $args, $echo = true) : string
	{
		// Declarar el directorio de plantillas twig (templates o views)
		Timber::$dirname = array('../templates', '../views');

		// Obtener contexto para timber
		$context = Timber::get_context();

		// Datos del plugin
		$context['plugin'] = array( 'directory' => plugin_dir_url(__FILE__) );

		// Render
		if ($echo) {
			return Timber::render($tpl, array_merge($context, $args));
		}
		// ¿o devolver la cadena?
		return Timber::fetch($tpl, array_merge($context, $args));
	}
}
