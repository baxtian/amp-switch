<?php
namespace AmpSwitch;

use Plugin_Upgrader;

/**
 * Administrar los plugins requeridos
 */
class PluginManagment {
	private $name;
	private $slug;
	private $zip;

	/**
	 * Constructor para crear unba instancia del plugin requerido
	 * @param string $name Nombre del plugin
	 * @param string $slug Identificador del plugin en el formato directorio/archivo-plugin.php
	 * @param string $zip  Enlace a la versión más reciente del plugin
	 */
	private function __construct( $name, $slug, $zip ) {
		$this->name = $name;
		$this->slug = $slug;
		$this->zip = $zip;
	}

	/**
	 * Revisa el estado del plugin y anexa mensajes para ser mostrados en la
	 * cabecera del escritorio de administración. Retorna verdadero en caso de
	 * haber publicado un mensaje o si ya había publicado anteriormente uno.
	 * El objetivo de esta función es filtrar los mensajes para que solo se vea un mensaje
	 * por dependencia y solo en el primero se vea el mensaje de instalación.
	 * @param  boolean $indicador_requerimiento_enviado ¿Mostrar el mensaje de dependencia?
	 * @return boolean                                  Nuevo estado del mensaje de dependencia.
	 */
	private function check($indicador_requerimiento_enviado = false) {
		// Variable global para saber si ya mostramos el mensaje para instalar requerimientos
		global $enlace_instalador_enviado;

		// ¿Se debe o no mostrar el mensaje? Es posible que ya se haya mostrado
		// el mensaje que indica la dependnecia así que no es necesario mostrarlo
		// nuevamente.
		if (!$indicador_requerimiento_enviado) {
			// Ya podemos dar por cierto que se va a mostrar el mensaje de
			// requrimiento de esta dependencia.
			$indicador_requerimiento_enviado = true;

			// ¿Estámos instalando los requerimientos?
			if (isset($_GET['req']) && $_GET['req'] == 'install') {
				add_action('admin_notices', function () {
					// Si se puede reemplazar/instalar el plugin indicamos que se
					// ha podido instalar correctamente
					if ($this->replace()) {
						$text = sprintf(__('%s has been activated succesfully.', ASW_D), $this->name);
						printf('<div class="notice notice-success is-dismissible"><p>%s</p></div>', $text);
					} else {
						// En caso de error indicar que es necesario instalar manualmente
						$text = sprintf(__("Couldn't install %s. Pleas try it manually.", ASW_D), $this->name);
						printf('<div class="error"><p>%s</p></div>', $text);
					}
				});
			} else { //Aun no estamos instalando, así que indicaremos los requerimientos
				//Si no está activo el plugin,
				if(!$this->is_active()) {
					// Ya se envió el mensaje para instalar los requerimientos
					// así que solo se muestra la dependencia
					if(isset($enlace_instalador_enviado) && $enlace_instalador_enviado) {
						add_action('admin_notices', function () {
							$text = sprintf(__("%s requires %s.", ASW_D), ASW_N, $this->name);
							printf('<div class="error"><p>%s</p></div>', $text);
						});
					} else { // Aun no se ha enviado el mensaje con el link par ainstalar los requerimientos
						// Dar por supuesto que ya vamos a enviar este enlace.
						$enlace_instalador_enviado = true;

						//Crear el mensaje con el enlace par ainstalar los requerimientos
						add_action('admin_notices', function () {
							$text = sprintf(__("%s requires %s. Use this link to automatically <a href='%s'>install requirements</a>.", ASW_D), ASW_N, $this->name, esc_url(admin_url('plugins.php?req=install')));
							printf('<div class="error"><p>%s</p></div>', $text);
						});
					}
				}
				// El caso contratía indicaría que está activo, así que es posible
				// que al ser requerido por otro plugin ya se haya instalado así
				// que podemos pasar este mensaje porque la dependencia ya se cumplió.
			}
		}

		return $indicador_requerimiento_enviado;
	}

	/**
	 * Reemplazar el plugin.
	 * @return boolean Estado del reemplazo.
	 */
	private function replace()
	{
		$status = false;

		if ($this->is_installed()) {
			$this->upgrade();
			$installed = true;
		} else {
			$installed = $this->install();
		}

		if (!is_wp_error($installed) && $installed) {
			$activate = activate_plugin($this->slug);

			if (is_null($activate)) {
				$status = true;
			}
		}

		return $status;
	}

	/**
	 * ¿Está activo el plugin?
	 * @return boolean ¿Está activo el plugin?
	 */
	private function is_active()
	{
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		return is_plugin_active($this->slug);
	}

	/**
	 * ¿Está instalado el plugin?
	 * @return boolean ¿Está instalado el plugin?
	 */
	private function is_installed()
	{
		if (! function_exists('get_plugins')) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}
		$all_plugins = get_plugins();

		if (!empty($all_plugins[$this->slug])) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Instalar el plugin
	 * @return boolean ¿Se pudo instalar el plugin?
	 */
	private function install()
	{
		require_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php';
		wp_cache_flush();

		$upgrader = new Plugin_Upgrader();
		$installed = $upgrader->install($this->zip);

		return $installed;
	}

	/**
	 * Actualizar el plugin
	 * @return boolean ¿Se pudo actualizar el plugin?
	 */
	private function upgrade()
	{
		require_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php';
		wp_cache_flush();

		$upgrader = new Plugin_Upgrader();
		$upgraded = $upgrader->upgrade($this->slug);

		return $upgraded;
	}

	/**
	 * Función para declarar los plugins requeridos
	 * @param  array $plugins Conjunto de plugins requeridos srcset. Cada fila corresponde a un plugin,
	 *                        y contendrá 3 items: 'name' para el nombre, 'slug' para el Identificador
	 *                        del plugin en el formato directorio/archivo-plugin.php, zip para el enlace
	 *                        a la versión más reciente del plugin y check para determinar cómo se sabrá si
	 *                        el plugin está instalado
	 *                        Ejemplo: array(
	 *                        	array(
	 *                        		'name' => 'Timber',
	 *                        		'slug' => 'timber-library/timber.php',
	 *                        		'zip'=> 'https://downloads.wordpress.org/plugin/timber-library.latest-stable.zip',
	 *                        		'check' => [ 'class_exists', 'Timber' ]
	 *                        	),
	 *                        	array(
	 *                        		'name' => 'SCSS-Library',
	 *                        		'slug' => 'scss-library/scss-library.php',
	 *                        		'zip'=> 'https://downloads.wordpress.org/plugin/scss-library.latest-stable.zip',
	 *                        		'check' => [ 'class_exists', 'ScssLibrary' ]
	 *                        	),
	 *                        )
	 */
	public static function requirements( $plugins ) {
		// Variable global con el arreglo para indicar si ya informamos sobre la
		// dependencia de un plugin particular.
		// Se declara global para compartir estos estados con otros manejadores
		// de dependencias.
		global $enlace_instalar;

		// Inicializar el arreglo en caso de ser necesario
		if(!$enlace_instalar) $enlace_instalar = array();

		// Inicializar el sistema de traducción
		load_plugin_textdomain(ASW_D, false, basename(dirname(__DIR__)) . '/languages');

		// Recorrer los requerimientos
		foreach($plugins as $plugin) {
			// Verificar que la función que teremina si está o no activo
			// el requerimiento retorne false (no está activo)
			$call = $plugin['check'][0];
			$args = $plugin['check'][1];
			if(!$call($args)) {
				// Inicializar la variable que tendrá la bandera que indica si ya se mostró
				// el enlace para instalar este plugin
				if(!isset($enlace_instalar[$plugin['name']])) $enlace_instalar[$plugin['name']] = false;

				// Inicializar instalador de plugin.
				$plug = new PluginManagment($plugin['name'], $plugin['slug'], $plugin['zip']);

				// Ejecutar instalador y guardar el estado que retorne en el arreglo
				// que guarda la bandera que indica si ya se mostró o no el mensaje sobre
				// de dependencia de este plugin.
				$enlace_instalar[$plugin['name']] = $plug->check($enlace_instalar[$plugin['name']]);
			}
		}

	}
}
