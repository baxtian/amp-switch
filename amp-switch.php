<?php
/*
Plugin Name: AMP-Switch
Description: Determina la platilla a ser visualizada al usar AMP
Author: Juan Sebastián Echeverry
Version: 0.1.0
Text Domain: amp-switch
Prefix: asw

Copyright 2017-2019 Juan Sebastián Echeverry (baxtian.echeverry@gmail.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

//Detectar la versión del plugin
$plugin_data = get_file_data(
	__FILE__,
	array(
		'name' => 'Plugin Name',
		'version' => 'Version',
		'text_domain' => 'Text Domain',
		'prefix' => 'Prefix',
	),
	false
);

define('ASW_N', $plugin_data['name']);
define('ASW_V', $plugin_data['version']);
define('ASW_D', $plugin_data['text_domain']);
define('ASW_P', $plugin_data['prefix']);

include_once(ABSPATH . 'wp-admin/includes/plugin.php');
require_once('vendor/autoload.php');

use AmpSwitch\PluginManagment as PluginManagment;

// Forzar uso de Timber y de SCSS_Library (en caso de ser requierido)
if (! is_plugin_active('timber-library/timber.php')) {
	PluginManagment::requirements(
		array(
			array(
				'name' => 'Timber',
				'slug' => 'timber-library/timber.php',
				'zip'=> 'https://downloads.wordpress.org/plugin/timber-library.latest-stable.zip',
				'check' => [ 'is_plugin_active', 'timber-library/timber.php' ]
			),
		)
	);

	return;
}

//Inicializar plugin
use AmpSwitch\Init as AmpSwitch;
AmpSwitch::get_instance();
